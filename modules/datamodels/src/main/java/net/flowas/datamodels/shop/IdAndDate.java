/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import lombok.Data;

/**
 *
 * @author liujh
 */
@Data
@MappedSuperclass
public abstract class IdAndDate implements Serializable{
        @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
        @Version
        private int version;
        @Temporal(TemporalType.TIMESTAMP)
        private Date createdAt;
        @Temporal(TemporalType.TIMESTAMP)
        private Date updatedAt;
}
