package net.flowas.modulecart.rest;

import javax.ws.rs.Path;

//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//@Produces(MediaType.APPLICATION_JSON)
public interface RootRest {
	@Path("/categories")
	CategoryEndpoint  getCategoryEndpoint();
	@Path("/products")
	ProductEndpoint getProductEndpoint();
	@Path("/configuration")
	ConfigerationEndpoint getConfigerationEndpoint();
}
